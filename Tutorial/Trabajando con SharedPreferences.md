# Trabajando con SharedPreferences

SharedPreferences es una interfaz para almacenar, acceder y modificar datos. Permite el almacenamiento en un conjunto de pares de llave - valor

En este tutorial crearemos una carpeta _storage_ en la fuente del proyecto y después un archivo Kotlin de nombre _AppPreferences_ dentro de la carpeta storage la cual debe contener lo siguiente:

``` kotlin

import android.content.Context
import android.content.SharedPreferences

class AppPreferences(ctx: Context) {
	var data: SharedPreferences = ctx.getSharedPreferences("APP_PREFERENCES", Context.MODE_PRIVATE)
	
	fun saveHighScore(highScore: Int) { 
	    data.edit().putInt("HIGH_SCORE", highScore).apply()
	}
	
	fun getHighScore(): Int {
		return data.getInt("HIGH_SCORE", 0)
	}
	
	fun clearHighScore() {
		data.edit().putInt("HIGH_SCORE", 0).apply()
	}
}
```
Esta clase se encargará de guardar el puntaje más alto obtenido por el jugador. 

Una vez creada esta clase ya podemos terminar la función de _MainActivity_

``` kotlin
private fun onBtnResetScoreClick(view: View) {
		val preferences = AppPreferences(this)
		preferences.clearHighScore()
}
```

Ahora cuando el usuario presione el botón de Reset Score, la puntuación más alta se inicializará en cero. Podemos utilizar _Snackbar_ para brindar al usuario una retroalimentación de lo que sucede.

Para poder utilizar _Snackbar_ necesitamos una librería y debe ser agregada en el script de construcción de **Gradle** a nivel del modulo en el apartado de dependencias. Esta debe coincidir con la version del _targetSdkVersion_

``` gradle
implementation	'com.android.support:design:28.0.0'
```

Depués de hacer esa alteración sincroniza tu proyecto y modifica la función _onBtnResetScoreClick_ para que ejecute la notificación al usuario.

``` kotlin
private fun onBtnResetScoreClick(view: View) {
		val preferences = AppPreferences(this)
		preferences.clearHighScore()
		Snackbar.make(view, "Score successfully reset", Snackbar.LENGTH_SHORT).show()
		tvHighScore?.text = "High score: ${preferences.getHighScore()}"
}
```

Ahora al presionar el botón de Reset Score mostrará el mensaje:

![reset](../resources/reset.png)