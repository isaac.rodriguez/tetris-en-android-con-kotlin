# Implementando el layout game activity

Ya hemos creado la interfaz de la pantalla principal, pero sí recordamos también es necesario implementar la pantalla donde se ejecuta el juego.

Ahora dirigete al archivo game_activity.xml y agrega lo siguiente:

```xml
<?xml	version="1.0"	encoding="utf-8"?>
<android.support.constraint.ConstraintLayout	xmlns:android="http://schemas.android.com/apk/res/andr
	xmlns:app="http://schemas.android.com/apk/res-auto"
	xmlns:tools="http://schemas.android.com/tools"
	android:layout_width="match_parent"
	android:layout_height="match_parent"
	tools:context="com.mydomain.tetris.GameActivity">
	<LinearLayout
		android:layout_width="match_parent"
		android:layout_height="match_parent"
		android:orientation="horizontal"
		android:weightSum="10"
		android:background="#e8e8e8">
		<LinearLayout
			android:layout_width="wrap_content"
			android:layout_height="match_parent"
			android:orientation="vertical"
			android:gravity="center"
			android:paddingTop="32dp"
			android:paddingBottom="32dp"
			android:layout_weight="1">
			<LinearLayout
	    		android:layout_width="wrap_content"
				android:layout_height="0dp"
				android:layout_weight="1"
				android:orientation="vertical"
				android:gravity="center">
				<TextView
					android:layout_width="wrap_content"
					android:layout_height="wrap_content"
					android:text="@string/current_score"
					android:textAllCaps="true"													
					android:textStyle="bold"
					android:textSize="14sp"/>
				<TextView
					android:id="@+id/tv_current_score"
					android:layout_width="wrap_content"
					android:layout_height="wrap_content"
					android:textSize="18sp"/>
				<TextView
					android:layout_width="wrap_content"
					android:layout_height="wrap_content"
					android:layout_marginTop="@dimen/layout_margin_top"
					android:text="@string/high_score"
					android:textAllCaps="true"
					android:textStyle="bold"
					android:textSize="14sp"/>
				<TextView
					android:id="@+id/tv_high_score"
					android:layout_width="wrap_content"
					android:layout_height="wrap_content"
					android:textSize="18sp"/>
					</LinearLayout>
		    			<Button
							android:id="@+id/btn_restart"
							android:layout_width="wrap_content"
							android:layout_height="wrap_content"
							android:text="@string/btn_restart"/>
			    	</LinearLayout>
				<View
					android:layout_width="1dp"
					android:layout_height="match_parent"
					android:background="#000"/>
				<LinearLayout
					android:layout_width="0dp"
					android:layout_height="match_parent"
					android:layout_weight="9">
				</LinearLayout>
		</LinearLayout>
</android.support.constraint.ConstraintLayout>
```

Agrega así mismo otras entradas al archivo de strings.xml

``` xml
<string	name="high_score">High score</string>
<string	name="current_score">Current score</string>
<string	name="btn_restart">Restart</string>
```

Y finalmente tenemos una logica simple en el archivo GameActivity:

``` kotlin
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.TextView
import com.mydomain.tetris.storage.AppPreferences

class GameActivity: AppCompatActivity() {

		var tvHighScore: TextView? = null
		var tvCurrentScore: TextView? = null
		var appPreferences: AppPreferences? = null
		
		public override fun onCreate(savedInstanceState: Bundle?) {
				super.onCreate(savedInstanceState)
				setContentView(R.layout.activity_game)
				appPreferences = AppPreferences(this)
				val	btnRestart = findViewById<Button>(R.id.btn_restart)
				tvHighScore = findViewById<TextView>(R.id.tv_high_score)
				tvCurrentScore = findViewById<TextView>(R.id.tv_current_score)
				updateHighScore()
				updateCurrentScore()
		}
		
		private fun updateHighScore() {
				tvHighScore?.text = "${appPreferences?.getHighScore()}"
		}
		
		private fun updateCurrentScore() {
				tvCurrentScore?.text = "0"
		}
}
```

Guarda todos los cambios y prueba que al presionar el botón de New Game te dirige a la pantalla de juego:

![Game](../resources/game.png)