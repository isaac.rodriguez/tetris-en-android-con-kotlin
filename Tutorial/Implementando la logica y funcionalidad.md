# Implementando la logica y funcionalidad

Primero debemos modelar las figuras ya que son un objeto crucial para el juego. Un bloque cuenta con las siguientes características:

 * Forma: Una forma que no puede ser cambiada.
 * Dimensiones: Una forma posee altura y anchura.
 * Color: Un color que se mantiene a lo largo de su existencia.
 * Características espaciales: Ocupa un lugar en el espacio.
 * Características posicionales: En algun momento en el tiempo, un bloque tiene una posición que existe entre los ejes X y Y
  
Para modelar un bloque necesitaremos una clase _helper_ que nos ayudará a generar un arreglo de bytes que utilizaremos para almacenar los bytes de los marcos.

Crea una carpeta _helpers_ en la fuente del proyecto, y dentro de la misma el archivo _HelperFunctions.kt_, lo abres e ingresas el siguiente código:

``` kotlin
fun array2dOfByte(sizeOuter: Int, sizeInner: Int): Array<ByteArray> = Array(sizeOuter) { ByteArray(sizeInner) }
```
Esta funcion recibe dos argumentos, el primero es el número de filas deseadas para crear el arreglo y el segundo argumento es el número de columnas deseadas para genera el arreglo. Al final se regresa el arreglo creado.

Ahora crea otro paquete en la fuente del proyecto con el nombre de _models_ y dentro la clase _Frame_ y agrega el siguiente código:

``` kotlin
import com.mydomain.tetris.helpers.array2dOfByte

class Frame(private val width: Int) {

		val data: ArrayList<ByteArray> = ArrayList()
		
		fun addRow(byteStr: String): Frame {
			val row = ByteArray(byteStr.length)
			for (index in byteStr.indices) {
				row[index] = "${byteStr[index]}".toByte()
			}
			data.add(row)
			return this
		}
		
		fun as2dByteArray(): Array<ByteArray> {
			val	bytes = array2dOfByte(data.size, width)
			return data.toArray(bytes)
		}
}
```

Habiendo modelado un marco adecuado para mantener nuestro bloque, podemos seguir adelante y modelar las distintas formas de posibles tetrominos en el juego.

Para lograr esto haremos uso de una clase _enum_. Crea un archivo _Shape.kt_ en el paquete _models_ y agrega lo siguiente para modelar cada figura:

``` kotlin
enum class Shape(val frameCount: Int, val startPosition: Int) {
    Tetromino(2,	2) {
        override fun getFrame(frameNumber: Int): Frame {
            return when	(frameNumber) {
                0 -> Frame(4).addRow("1111")
                1 -> Frame(1)
                        .addRow("1")
                        .addRow("1")
                        .addRow("1")
                        .addRow("1")
                else -> throw IllegalArgumentException("$frameNumber is an invalid frame number.")
            }
        }
    },

    Tetromino1(1, 1)	{
        override fun getFrame(frameNumber: Int): Frame	{
            return Frame(2)
                    .addRow("11")
                    .addRow("11")
        }
    },

    Tetromino2(2, 1)	{
        override fun getFrame(frameNumber: Int): Frame	{
            return when (frameNumber) {
                0 -> Frame(3)
                        .addRow("110")
                        .addRow("011")
                1 -> Frame(2)
                        .addRow("01")
                        .addRow("11")
                        .addRow("10")
                else -> throw IllegalArgumentException("$frameNumber is an invalid frame number.")
            }
        }
    },

    Tetromino3(2, 1)	{
        override fun getFrame(frameNumber: Int): Frame {
            return when (frameNumber) {
                0 -> Frame(3)
                        .addRow("011")
                        .addRow("110")
                1 -> Frame(2)
                        .addRow("10")
                        .addRow("11")
                        .addRow("01")
                else ->	throw IllegalArgumentException("$frameNumber is an invalid frame number.")
            }
        }
    },

    Tetromino4(2, 2)	{
        override fun getFrame(frameNumber: Int): Frame	{
            return when (frameNumber) {
                0 -> Frame(4).addRow("1111")
                1 -> Frame(1)
                        .addRow("1")
                        .addRow("1")
                        .addRow("1")
                        .addRow("1")
                else -> throw IllegalArgumentException("$frameNumber is an invalid frame number.")
            }
        }
    },

    Tetromino5(4, 1) {
        override fun getFrame(frameNumber: Int): Frame {
            return when (frameNumber) {
                0 -> Frame(3)
                        .addRow("010")
                        .addRow("111")
                1 -> Frame(2)
                        .addRow("10")
                        .addRow("11")
                        .addRow("10")
                2 -> Frame(3)
                        .addRow("111")
                        .addRow("010")
                3 -> Frame(2)
                        .addRow("01")
                        .addRow("11")
                        .addRow("01")
                else -> throw IllegalArgumentException("$frameNumber is an invalid frame number.")
            }
        }
    },

    Tetromino6(4, 1)	{
        override fun getFrame(frameNumber: Int): Frame {
            return when (frameNumber) {
                0 -> Frame(3)
                        .addRow("100")
                        .addRow("111")
                1 -> Frame(2)
                        .addRow("11")
                        .addRow("10")
                        .addRow("10")
                2 -> Frame(3)
                        .addRow("111")
                        .addRow("001")
                3 -> Frame(2)
                        .addRow("01")
                        .addRow("01")
                        .addRow("11")
                else -> throw IllegalArgumentException("$frameNumber is an invalid frame number.")
            }
        }
    },

    Tetromino7(4, 1)	{
        override fun getFrame(frameNumber: Int): Frame {
            return when (frameNumber) {
                0 -> Frame(3)
                        .addRow("001")
                        .addRow("111")
                1 -> Frame(2)
                        .addRow("10")
                        .addRow("10")
                        .addRow("11")
                2 -> Frame(3)
                        .addRow("111")
                        .addRow("100")
                3 -> Frame(2)
                        .addRow("11")
                        .addRow("01")
                        .addRow("01")
                else -> throw IllegalArgumentException("$frameNumber is an invalid frame number.")
            }
        }
    };

    abstract fun getFrame(frameNumber: Int): Frame
}
```
Habiendo modelado tanto el marco como la forma del bloque, lo siguiente que debemos modelar mediante programación es el propio bloque. Utilizaremos esto como una oportunidad para demostrar la interoperabilidad perfecta de Kotlin con Java implementando el modelo con Java.

Crea una clase Java dentro del paquete _models_ con el nombre de _Block_ y agrega el siguiente código:

``` java
import android.graphics.Color;
import android.graphics.Point;

public class Block {

	private int shapeIndex;
	private int frameNumber;
	private BlockColor color;
	private Point position;
	
	public enum BlockColor {
		PINK(Color.rgb(255, 105, 180), (byte) 2),
		GREEN(Color.rgb(0, 128, 0), (byte) 3),
		ORANGE(Color.rgb(255, 140, 0), (byte) 4),
		YELLOW(Color.rgb(255, 255, 0), (byte) 5),
		CYAN(Color.rgb(0, 255, 255), (byte) 6);
						
		BlockColor(int rgbValue, byte value) {
			this.rgbValue = rgbValue;
			this.byteValue = value;
		}
		
		private final int rgbValue;
		private final byte byteValue;
	}
}
``` 
En el bloque de código anterior, agregamos cuatro variables de instancia: shapeIndex, frameNumber, color y position. shapeIndex
mantendrá el índice de la forma del bloque, frameNumber hará un seguimiento del número de cuadros que tiene la forma del bloque, el color mantendrá el color característico del bloque, y la posición se usará para mantener un registro de la posición espacial actual del bloque en el campo del juego.

Ahora debemos definir un constructor privado porque no queremos que sea accesible desde fuera de la clase _Block_, como todavía queremos que otras clases tengan un medio para crear una instancia de bloque, tenemos que definir un método estático que permita esto. Agregamos el siguiente código:

``` java
private	Block(int shapeIndex, BlockColor blockColor) {
        this.frameNumber = 0;
        this.shapeIndex = shapeIndex;
        this.color = blockColor;
        this.position = new Point( FieldConstants.COLUMN_COUNT.getValue() / 2, 0);
    }

    public static Block createBlock() {
        Random random = new Random();
        int shapeIndex = random.nextInt(Shape.values().length);
        BlockColor blockColor = BlockColor.values()[random.nextInt(BlockColor.values().length)];
        Block block	= new Block(shapeIndex, blockColor);
        block.position.x = block.position.x - Shape.values()[shapeIndex].getStartPosition();
        return	block;
    }
    
    public enum BlockColor {
        PINK(Color.rgb(255,	105,	180), (byte) 2),
        GREEN(Color.rgb(0,	128,	0), (byte) 3),
        ORANGE(Color.rgb(255,	140,	0), (byte)	4),
        YELLOW(Color.rgb(255,	255,	0), (byte) 5),
        CYAN(Color.rgb(0,	255,	255), (byte) 6);

        BlockColor(int rgbValue, byte value) {
            this.rgbValue = rgbValue;
            this.byteValue = value;
        }
        private	final int rgbValue;
        private	final byte byteValue;
    }
```
Necesitamos agregar algunos métodos get y set a Block. Estos métodos darán acceso a propiedades cruciales de las instancias del bloque.

``` java
public static int getColor(byte	value) {
        for (BlockColor colour : BlockColor.values()) {
            if	(value == colour.byteValue) {
                return colour.rgbValue;
            }
        }
        return	-1;
    }

    public final void setState(int frame, Point	position) {
        this.frameNumber = frame;
        this.position =	position;
    }

    @NonNull
    public final byte[][] getShape(int frameNumber)	{
        return Shape.values()[shapeIndex].getFrame(frameNumber).as2dByteArray();
    }

    public Point getPosition() {
        return this.position;
    }

    public final int getFrameCount() {
        return Shape.values()[shapeIndex].getFrameCount();
    }

    public int getFrameNumber() {
        return frameNumber;
    }

    public int getColor() {
        return color.rgbValue;
    }

    public byte getStaticValue() {
        return color.byteValue;
    }
```

_@NoNull_ es una anotación proporcionada por el marco de la aplicación de Android que denota que un campo, parámetro o retorno de método nunca puede ser nulo. No olvides añadir el paquete que da acceso a las anotaciones:

``` java
import android.support.annotation.NonNull;
```
Hay una última cosa de la que deberíamos ocuparnos en la clase Block antes de continuar. En la línea final del constructor de bloques, la posición de la variable de instancia de posición de la instancia de bloque actual se establece de la siguiente manera:

``` java
this.position = new Point( FieldConstants.COLUMN_COUNT.getValue() / 2, 0);
``` 
El 10 es el recuento de columnas del campo en el que se generarán los tetrominos. Este es un valor constante que se utilizará varias veces dentro del código para esta aplicación, y como tal, se declara mejor como una constante. Cree un paquete denominado _constants_ en el paquete fuente de la aplicación base y agregue un nuevo archivo Kotlin con el nombre _FieldConstants_ al paquete. A continuación, agregue constantes para el número de columnas y filas que el campo de juego tendrá. El campo debe poseer diez columnas y veinte filas:

``` kotlin
enum class FieldConstants(val value: Int) {
	COLUMN_COUNT(10), ROW_COUNT(20);
}
```

Importa el paquete de FieldConstants a la clase java _Block_ para que ya no marque el error.