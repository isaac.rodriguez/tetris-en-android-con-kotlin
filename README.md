# **Tutorial de Tetris para Android con Kotlin**

# Contenido

 * Introducción
   * Android
   * Tetris
 * Requisitos
 * Instalación

## Introducción

La mejor forma de aprender un lenguaje de programación es a travez de ejemplos reales donde se apliquen varios conceptos básicos y avanzados del lenguaje a aprender, Kotlin es mayormente utilizado para desarrollar aplicaciones Android, en este tutorial realizaremos un juego Tetris en forma de una aplicación Android para aprender el lenguaje, pero antes de continuar es necesario conocer algunos conceptos

### Android

Android es un sistema operativo móvil basado en Linux desarrollado y mantenido por Google y creado principalmente para impulsar dispositivos móviles inteligentes tales como teléfonos móviles y tabletas.

El software se puede instalar en el sistema operativo Android en forma de apps. Una app es una aplicación que se ejecuta dentro de un entorno y realiza una o más tareas para el logro de un objetivo o una colección de metas. Estas aplicaciones pueden dirigirse a diferentes mercados, como recreación, empresa y comercio electrónico. Las aplicaciones también pueden venir en forma de juegos.

### Tetris

Tetris es un videojuego de rompecabezas que hace juego con fichas. El nombre Tetris se deriva de las palabras tetra, el prefijo numérico griego para cuatro, y tenis. Las tejass en Tetris se combinan para formar tetrominos que son formas geométricas compuestas de cuatro cuadrados conectados ortogonalmente.

En Tetris, una secuencia aleatoria de tetrominos cae sobre un campo de juego. Estos tetrominos pueden ser manipulados por el jugador. Se pueden realizar varios movimientos en cada pieza de tetromino. Las piezas se pueden mover hacia la izquierda, hacia la derecha y rotar. Además, se puede acelerar la velocidad de descenso de cada pieza. El objetivo del juego es crear una línea horizontal ininterrumpida de diez celdas con las piezas descendentes. Cuando se crea una línea de este tipo, la línea se borra.

## Requisitos

 1. Conocimientos básicos del lenguaje de programación Kotlin, tales como declaración de variables, tipos de datos, arreglos, clases, programación orientada a objetos, declaracion de funciones, etc.
 2. Conocimientos básicos de programación en Android, aunque se revisarn algunos conceptos básicos.
 3. De preferencia se recomienda el uso de Android Studio ya que cuenta con herramiientas que facilitan el desarrollo, así mismo desde Android Studio 3.0 se incluyen compatibilidad probada con Kotlin.

## Instalación

Simplemente clona o descarga el repositorio para tu proyecto Android en Android Studio 3.0 en adelante.